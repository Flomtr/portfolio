<?php


namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ProjectController extends AbstractController
{
    /**
     * @Route("/projet", name="app_project")
     */
    public function index() {

        return $this->render("/project/projects.html.twig");

    }
    /**
     * @Route("/projet/avenria", name="app_project_avenria")
     */
    public function avenria() {

        return $this->render("/project/avenria.html.twig");

    }
}