<?php


namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="app_contact")
     * @param Request $request
     * @param MailerInterface $mailer
     * @return Response
     * @throws TransportExceptionInterface
     */
    public function index(Request $request, MailerInterface $mailer): Response
    {
        $contact = new Contact();
        $contact->setCreatedAt(new \DateTime());
        $form = $this->createForm(ContactFormType::class, $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $contactModel = $form->getData();

            $email = (new Email())
                ->from($contact->getEmail())
                ->to('mottura.florian@gmail.com')
                ->subject('Email du Portfolio')
                ->priority(Email::PRIORITY_HIGH)
                ->text("Nom : ". $contactModel->getName() . " " . "Message : " . $contactModel->getMessage());

            $mailer->send($email);
            $this->addFlash('success','Votre message à bien été envoyé !');
        }

        return $this->render('pages/contact.html.twig', [
            'form' => $form->createView()
        ]);
    }
}