import '../css/app.scss';
import "particles.js";
import "scrollreveal";
import ScrollReveal from "scrollreveal";
import './particles/particles-config';
import Typewriter from 'typewriter-effect/dist/core';

let divType = document.getElementById('typewriter-hello');
let divTypewriter = document.getElementById('typewriter');

if ((divType) && (divTypewriter)) {

    let type = new Typewriter('#typewriter-hello', {
        loop: false,
        delay: 100,
    });
    let typewriter = new Typewriter('#typewriter', {
        loop: false,
        delay: 80,
    });

    type
        .pauseFor(1000)
        .typeString('Florian Mottura')
        .start();

    typewriter
        .pauseFor(4000)
        .typeString('Je développe des Sites Web')
        .pauseFor(1000)
        .deleteChars(9)
        .typeString('Applications Web')
        .pauseFor(1000)
        .deleteAll()
        .typeString('Bienvenue sur mon Portfolio !')
        .pauseFor(1000)
        .start();
}

const sr = ScrollReveal({
    duration: 1000,
    distance: "30px",
    reset: false

});

sr.reveal(".hero__myself", {
    origin: "top"
});
sr.reveal(".hero__name", {
    delay: 1000,
    origin: "bottom"
});
sr.reveal(".show-in");

